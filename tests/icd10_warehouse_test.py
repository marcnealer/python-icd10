import sys
import os

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../src')))

import unittest
import pathlib
from tinydb.table import Document
from icd10_load import ICD10Load
from icd10_warehouse import ICD10Warehouse


class TestICD10Warehouse(unittest.TestCase):
    def setUp(self):
        self.warehouse_path = pathlib.Path("/tmp/test/warehouse")
        self.icd10load = ICD10Load(self.warehouse_path)
        self.icd10load.load_from_cdc()
        self.icd10 = ICD10Warehouse(self.warehouse_path)

    def test_warehouse(self):
        search_recs = self.icd10.search_description("arthritis")
        self.assertIsInstance(search_recs, list, "Description search should have returned a list")
        for search_rec in search_recs:
            self.assertIsInstance(search_rec, Document)
            self.assertIn("arthritis", search_rec["description"].lower(),
                          "Search failed. 'arthritis' not in description")

        recs = self.icd10.search_by_code("M30")
        self.assertIsInstance(recs, list, "Search by Code did not return a list")
        for rec in recs:
            self.assertIsInstance(rec, Document)
            self.assertIn("m30", rec["name"].lower(), "Search By code returned a bad record")

        rec = self.icd10.get_by_code("M30")
        self.assertIsInstance(rec, Document)
        self.assertEqual(rec["name"].lower(), "m30")

        recs = self.icd10.get_all_codes()
        self.assertIsInstance(recs, list)
        [self.assertIsInstance(rec, str) for rec in recs]

    def tearDown(self):
        # Cleanup after tests (optional)
        db_path = self.warehouse_path / "icd10_warehouse.json"
        if db_path.exists():
            db_path.unlink()


if __name__ == '__main__':
    unittest.main()
