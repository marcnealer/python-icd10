import sys
import os

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../src')))
import unittest
import pathlib
from tinydb.table import Document
from icd10_load import ICD10Load  # Assuming the ICD10Load class is in icd10_load.py


class TestICD10Load(unittest.TestCase):

    def setUp(self):
        # Create a ICD10Load object with a specific path
        self.warehouse_path = pathlib.Path("/tmp/test/warehouse")
        self.icd10load = ICD10Load(self.warehouse_path)

    def test_load_from_cdc(self):
        # Call the method to be tested
        try:
            self.icd10load.load_from_cdc()
            operation_successful = True
        except Exception:
            operation_successful = False

        # Check that the method executed without exceptions
        self.assertTrue(operation_successful, "load_from_cdc operation failed.")
        try:
            records = self.icd10load.db.all()
            extract_worked = True
        except Exception:
            records = None
            extract_worked = False
        self.assertTrue(extract_worked, "Loading records fom the warehouse failed")

        self.assertIsInstance(records, list)
        for rec in records:
            self.assertIsInstance(rec, Document)

        test_rec = records[0]

        self.assertIsInstance(test_rec.doc_id, int)
        self.assertIn('name', test_rec, "Data structure Error. Name is missing")
        self.assertIsInstance(test_rec["name"], str, "Data Structure Error: Name must be a string.")
        self.assertIn("description", test_rec, "Data structure Error. Description is missing.")
        self.assertIsInstance(test_rec["description"], str, "Data Structure Error: Description must be a string.")
        self.assertIn("excludes1", test_rec, "Data structure Error. exclude1 is missing.")
        self.assertIsInstance(test_rec["excludes1"], (str, list),
                              "Data Structure Error: exclude1 must be a string.")
        self.assertIn("excludes2", test_rec, "Data structure Error. exclude2 is missing.")
        self.assertIsInstance(test_rec["excludes2"], (str, list),
                              "Data Structure Error: exclude2 must be a string.")
        self.assertIn("includes", test_rec, "Data structure Error. include is missing.")
        self.assertIsInstance(test_rec["includes"], (str, list)
                              , "Data Structure Error: include must be a string.")
        self.assertIn("code_first", test_rec, "Data structure Error. code_first is missing.")
        self.assertIsInstance(test_rec["code_first"], (str, list),
                              "Data Structure Error: code_first must be a string.")
        self.assertIn("additional_codes", test_rec, "Data structure Error. additional_codes is missing.")
        self.assertIsInstance(test_rec["additional_codes"], (str, list),
                              "Data Structure Error: additional_codes must be a string.")
        self.assertIn("parent", test_rec, "Data structure Error. parent is missing.")
        self.assertIn("section_name", test_rec, "Data structure Error. section_name is missing.")
        self.assertIsInstance(test_rec["section_name"], str, "Data Structure Error: section_name must be a string.")
        self.assertIn("section_description", test_rec,
                      "Data structure Error. section_description is missing.")
        self.assertIsInstance(test_rec["section_description"], str,
                              "Data Structure Error: section_description must be a string.")

    def tearDown(self):
        # Cleanup after tests (optional)
        db_path = self.warehouse_path / "icd10_warehouse.json"
        if db_path.exists():
            db_path.unlink()


if __name__ == '__main__':
    unittest.main()
